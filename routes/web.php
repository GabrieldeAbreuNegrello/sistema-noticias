<?php

// Seção Home

Route::get('/', function () {
    return view('home.index');
});


Route::get('/contato', function () {
    return view('home.contato');
});

// Seção Noticia

Route::get('/tecnologia', function () {
    return view('noticias.index');
});

Route::get('/tecnologia/titulo-noticia', function () {
    return view('noticias.visualizar');
});


// Seção Admin

Route::get('/admin/home', function () {
    return view('admin.home.index');
});

//Seção Admin/noticias

Route::get('/admin/noticias', function () {
    return view('admin.noticias.index');
});

Route::get('/admin/noticias/cadastrar', function () {
    return view('admin.noticias.cadastrar');
});

Route::get('/admin/noticias/editar', function () {
    return view('admin.noticias.editar');
});

Route::get('/admin/noticias/visualizar', function () {
    return view('admin.noticias.visualizar');
});

//Seção Admin/categorias



//Seção Admin/usuarios


