@extends('layouts.admin')

@section('titulo','Área administrativa')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Visualizar Notícia</h2>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
           <table class="table table-striped table-condensed">
               <tr>
                    <th width="150">ID</th>
                    <td>1</td>
               </tr>
               <tr>
                    <th width="150">Título</th>
                    <td>Flamengo ganhou do corinthias</td>
               </tr>
               <tr>
                    <th width="150">Subtitulo</th>
                    <td>Tem jogo de Volta</td>
               </tr>
               <tr>
                    <th width="150">Descrição</th>
                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur suscipit quaerat iste quae, quisquam porro esse dicta eius hic culpa, accusantium eaque ipsam neque ut consectetur obcaecati perspiciatis assumenda quibusdam.</td>
               </tr>
               <tr>
                    <th width="150">Status</th>
                    <td>Não Publicado</td>
               </tr>
           </table>
           <a href="#" class="btn btn-danger">Editar Notícia</a>
        <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
        
    </div>

</div>


@endsection